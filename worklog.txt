
I am starting to work through Ray Tracing in One Weekend

1. Overview

The first thing I learned was that it was possible to create awesome images without an API. Additionally, while I am not surprised, I did not know that most video games and movie renderers are written in C++, but with what we have learned throughout this semester, it makes more sense. This was just the overview, but I am excited to get started with this project, and keep down everything that I learn written here. 

2.1 The PPM Image Format

It makes sense that to start a renderer, we need a way to see the colors. With this first snippet of code, we are printing out RGB values and are able to generate our first image. Additionally something I learned was that we can produce an array of color values with just red, green, and blue. 

2.2 Creating an Image File 

Awesome to see how the values align to create this color palette. 

2.3 Adding a progress indicator

The progress indicator is a great addition not only to this ray tracing program but many programs involving long complicated for loops that run for a long time. If we stall out due to an infinite for loop we want to be able to see on which iteration we did so. 

3. Vec 3 Class Overview

The fact that we can use the same class for colors, location, offset, etc makes the use case for classes abundantly clear. Always take the less complicated route as long as it doesn't produce any errors or shortcomings. 

3.1 Variables and Methods

Created a vec 3 class. This represents a 3D cartesian vector/ 

3.2 vec 3 Utility Functions 

This is writing a cartesian vector utility function that we will use often. 

3.3 Color Utility Functions 

With the addition of the color utility, we are able to create the final code for the first image. 

4.1 Ray Class 

In this section we built out the ray class. Added

4.2 Sending Rays Into the scene

With this code we make a ray tracer, and the output changes as follows, and our .ppm image output is a blue and white gradient. Something I learned was a graphics trick that keeps the colors blue and white.

Image and camera code:     // Image
    const auto aspect_ratio = 16.0 / 9.0;
    const int image_width = 400;
    const int image_height = static_cast<int>(image_width / aspect_ratio);

    // Camera

    auto viewport_height = 2.0;
    auto viewport_width = aspect_ratio * viewport_height;
    auto focal_length = 1.0;

    auto origin = point3(0, 0, 0);
    auto horizontal = vec3(viewport_width, 0, 0);
    auto vertical = vec3(0, viewport_height, 0);
    auto lower_left_corner = origin - horizontal/2 - vertical/2 - vec3(0, 0, focal_length);

Ray color code:
color ray_color(const ray& r) {
    vec3 unit_direction = unit_vector(r.direction());
    auto t = 0.5*(unit_direction.y() + 1.0);
    return (1.0-t)*color(1.0, 1.0, 1.0) + t*color(0.5, 0.7, 1.0);
} By changing this snippet we can modify the colors of our gradient 

5.1 Ray-Sphere Intersection 

We are about to add an object to our ray tracer. In this section, I learned that you always want formulas to be in terms of vectors so that it works with the vec 3 class. The unknown we are solving for is t. After this step we will hard code this given equation. 

No new code added

5.2 Creating Our First Raytraced Image

New code: 
bool hit_sphere(const point3& center, double radius, const ray& r) {
    vec3 oc = r.origin() - center;
    auto a = dot(r.direction(), r.direction());
    auto b = 2.0 * dot(oc, r.direction());
    auto c = dot(oc, oc) - radius*radius;
    auto discriminant = b*b - 4*a*c;
    return (discriminant > 0);
}
  if (hit_sphere(point3(0,0,-1), 0.5, r))
        return color(1, 0, 0);

This code will allow us to render a red sphere, and it matches as expected in the image output. We can manipulate the numerical values if we wanted to get additional colors. This is only one object, but is a cool demonstration of how to generate a sphere .

Chapter 6: Surface Normals and Multiple Objects 

6.1 Shading With Surface Normals 

With this new code we are enhancing our original red sphere that we left off with at the end of chapter 5. 

New code implemented: 
double hit_sphere(const point3& center, double radius, const ray& r) {
    vec3 oc = r.origin() - center;
    auto a = dot(r.direction(), r.direction());
    auto b = 2.0 * dot(oc, r.direction());
    auto c = dot(oc, oc) - radius*radius;
    auto discriminant = b*b - 4*a*c;
    if (discriminant < 0) {
        return -1.0;
    } else {
        return (-b - sqrt(discriminant) ) / (2.0*a);
    }
}

color ray_color(const ray& r) {
    auto t = hit_sphere(point3(0,0,-1), 0.5, r);
    if (t > 0.0) {
        vec3 N = unit_vector(r.at(t) - vec3(0,0,-1));
        return 0.5*color(N.x()+1, N.y()+1, N.z()+1);
    }
    vec3 unit_direction = unit_vector(r.direction());
    t = 0.5*(unit_direction.y() + 1.0);
    return (1.0-t)*color(1.0, 1.0, 1.0) + t*color(0.5, 0.7, 1.0);
}
With this code we map x y z to r/g/b, which is the most efficient way to do so. Additionally with our first IF statement, we map each component from zero to one .

6.2: Simplifying the Ray-Sphere Intersection Code

We are simplifying our previously written ray-sphere intersection by realizing that a vector dotted with itself is equal to the squared length of that vector.

New code (written over previous code) : 

double hit_sphere(const point3& center, double radius, const ray& r) {
    vec3 oc = r.origin() - center;
    auto a = r.direction().length_squared();
    auto half_b = dot(oc, r.direction());
    auto c = oc.length_squared() - radius*radius;
    auto discriminant = half_b*half_b - a*c;

    if (discriminant < 0) {
        return -1.0;
    } else {
        return (-half_b - sqrt(discriminant) ) / a;
    }
}

This has not changed our current image output. 

6.3: An Abstraction For Hittable Objects 

Instead of writing an array of spheres it makes sense to abstract out a class to make the spheres easier to work with. The hittable class acts as an object:
#ifndef HITTABLE_H
#define HITTABLE_H

#include "ray.h"

struct hit_record {
    point3 p;
    vec3 normal;
    double t;
};

class hittable {
    public:
        virtual bool hit(const ray& r, double t_min, double t_max, hit_record& rec) const = 0;
};

#endif

Something I learned with this code is that if you have a bunch of different possible computations, it makes sense to store these different possibilities within a structure.

6.4: Front Faces Versus Back Faces 

We need to determine which side of the surface that the ray is coming from. 

New code: 
if (dot(ray_direction, outward_normal) > 0.0) {
    // ray is inside the sphere
    ...
} else {
    // ray is outside the sphere
    ...
}

If the normals always point out we do X; otherwise we do Y. (This compares the ray and the normal). 

New code: 
bool front_face;
if (dot(ray_direction, outward_normal) > 0.0) {
    // ray is inside the sphere
    normal = -outward_normal;
    front_face = false;
} else {
    // ray is outside the sphere
    normal = outward_normal;
    front_face = true;
}

We need to be able to determine and remember which side of the surface that the ray is on, this code suffices for such.


6.5: A List of Hittable Objects 

The hittable object is implemented as generic. New code:

class hittable_list : public hittable {
    public:
        hittable_list() {}
        hittable_list(shared_ptr<hittable> object) { add(object); }

        void clear() { objects.clear(); }
        void add(shared_ptr<hittable> object) { objects.push_back(object); }

        virtual bool hit(
            const ray& r, double t_min, double t_max, hit_record& rec) const override;

    public:
        std::vector<shared_ptr<hittable>> objects;
};

bool hittable_list::hit(const ray& r, double t_min, double t_max, hit_record& rec) const {
    hit_record temp_rec;
    bool hit_anything = false;
    auto closest_so_far = t_max;

    for (const auto& object : objects) {
        if (object->hit(r, t_min, closest_so_far, temp_rec)) {
            hit_anything = true;
            closest_so_far = temp_rec.t;
            rec = temp_rec;
        }
    }

    return hit_anything;
}

With this code we have constructed a class, and we are now able to store a list of hittable objects. 

6.6: Some New C++ Features

New concept: A shared_ptr<type> is a pointer to some allocated type, with reference-counting semantics.

New code (example allocation using a shared pointer): shared_ptr<double> double_ptr = make_shared<double>(0.37);
shared_ptr<vec3>   vec3_ptr   = make_shared<vec3>(1.414214, 2.718281, 1.618034);
shared_ptr<sphere> sphere_ptr = make_shared<sphere>(point3(0,0,0), 1.0);

Initializing a shared pointer with a new object. That being said, the type can be automatically deduced by the program, so the following format is more suitable: 

auto double_ptr = make_shared<double>(0.37);
auto vec3_ptr   = make_shared<vec3>(1.414214, 2.718281, 1.618034);
auto sphere_ptr = make_shared<sphere>(point3(0,0,0), 1.0);

Shared pointers allow multiple geometries to share a common singular instance


6.7: Common Constants and Utility Functions

In this section we are defining constants that we consistently use throughout our program. 

New code: 

const double infinity = std::numeric_limits<double>::infinity();
const double pi = 3.1415926535897932385;

We hardcode the pi variable and define infinity.

New inclusions on main :
#include "rtweekend.h"

#include "color.h"
#include "hittable_list.h"
#include "sphere.h"

New code:
color ray_color(const ray& r, const hittable& world) {
    hit_record rec;
    if (world.hit(r, 0, infinity, rec)) {
        return 0.5 * (rec.normal + color(1,1,1));
    }
    vec3 unit_direction = unit_vector(r.direction());
    auto t = 0.5*(unit_direction.y() + 1.0);
    return (1.0-t)*color(1.0, 1.0, 1.0) + t*color(0.5, 0.7, 1.0);
}


    // World
    hittable_list world;
    world.add(make_shared<sphere>(point3(0,0,-1), 0.5));
    world.add(make_shared<sphere>(point3(0,-100.5,-1), 100));

            color pixel_color = ray_color(r, world);


This presents a new main with a list of hittable objects and a new picture. 


7.0: Antialiasing Overview

We do not want jaggies around the edges of pixels (such as a real camera mitigates), so to do this we will average a bunch of samples in each pixel.


7.1: Some Random Number Utilities

New code:

#include <random>

inline double random_double() {
    static std::uniform_real_distribution<double> distribution(0.0, 1.0);
    static std::mt19937 generator;
    return distribution(generator);
}

Random number generator that returns REAL random numbers using the new random library.

7.2: Generating Pixels With Multiple Samples 

New code: #ifndef CAMERA_H
#define CAMERA_H

#include "rtweekend.h"

class camera {
    public:
        camera() {
            auto aspect_ratio = 16.0 / 9.0;
            auto viewport_height = 2.0;
            auto viewport_width = aspect_ratio * viewport_height;
            auto focal_length = 1.0;

            origin = point3(0, 0, 0);
            horizontal = vec3(viewport_width, 0.0, 0.0);
            vertical = vec3(0.0, viewport_height, 0.0);
            lower_left_corner = origin - horizontal/2 - vertical/2 - vec3(0, 0, focal_length);
        }

        ray get_ray(double u, double v) const {
            return ray(origin, lower_left_corner + u*horizontal + v*vertical - origin);
        }

    private:
        point3 origin;
        point3 lower_left_corner;
        vec3 horizontal;
        vec3 vertical;
};
#endif

This is a camera class that manages our virtual camera and related tasks. 

New code (main):
#include "camera.h" 

 Camera cam; 

            color pixel_color(0, 0, 0);
            for (int s = 0; s < samples_per_pixel; ++s) {
                auto u = (i + random_double()) / (image_width-1);
                auto v = (j + random_double()) / (image_height-1);
                ray r = cam.get_ray(u, v);
                pixel_color += ray_color(r, world);
            }
            write_color(std::cout, pixel_color, samples_per_pixel);


This new code allows our image to produce a new image with a substantial difference in edge pixels. We do so because we do not wanted jagged edges later on in in the program.


Chapter 8.1: A Simple Diffuse Material 

The darker that the diffuse surface is, the more likely that the rays are absorbed by the surface. The sphere with a center at (𝐏−𝐧) is considered inside the surface, whereas the sphere with center (𝐏+𝐧) is considered outside the surface.

New code:

class vec3 {
  public:
    ...
    inline static vec3 random() {
        return vec3(random_double(), random_double(), random_double());
    }

    inline static vec3 random(double min, double max) {
        return vec3(random_double(min,max), random_double(min,max), random_double(min,max));
    }

This code picks a random point in the unit cube where x, y, and z all range from −1 to +1

Updated code:
if (world.hit(r, 0, infinity, rec)) {
        point3 target = rec.p + rec.normal + random_in_unit_sphere();
        return 0.5 * ray_color(ray(rec.p, target - rec.p), world);
    }

This uses the new random direction generator. 

8.2: Limiting the Number of Child Rays

The ray color function that we previously implemented is recursive, so we need to limit the maximum recursion depth as to avoid light contribution beyond the maximum depth: 

New code :

color ray_color(const ray& r, const hittable& world, int depth) {
    hit_record rec;

    // If we've exceeded the ray bounce limit, no more light is gathered.
    if (depth <= 0)
        return color(0,0,0);

    if (world.hit(r, 0, infinity, rec)) {
        point3 target = rec.p + rec.normal + random_in_unit_sphere();
        return 0.5 * ray_color(ray(rec.p, target - rec.p), world, depth-1);
    }

  New code in main:
  const int max_depth = 50;

                pixel_color += ray_color(r, world, max_depth);


This code allows us to limit the depth.


8.3 : Using Gamma Correction for Accurate Color Intensity

New code:

    // Divide the color by the number of samples and gamma-correct for gamma=2.0.
    auto scale = 1.0 / samples_per_pixel;
    r = sqrt(scale * r);
    g = sqrt(scale * g);
    b = sqrt(scale * b);


This code lightens the very dark grey sphere that we originally had (due to absorbing half the energy on each bounce). 

8.4 : Fixing Shadow Acne

New code :

if (world.hit(r, 0.001, infinity, rec)) {


This fixes a bug where we are reflecting off points that are EXTREMELY CLOSE to zero but not 0 due to floating point approximation .This ignores hits that are close to zero. 


8.5 True Lambertian Reflection

In this section I learned about the lambertian distribution for the first time, which allows for a more uniform distribution 

New code (random unit vector function) : 
vec3 random_unit_vector() {
    return unit_vector(random_in_unit_sphere());
}


The new code produces an image that is almost identical to the most recent image, with a few subtle differences. The shadows are not as intense, and the spheres both appear slightly lighter (due to being diffuse objects). 

8.6: An Alternative Diffuse Formulation 

Up until now we have been unable to realize that our diffuse method was an incorrect approximation. 

New code: 

vec3 random_in_hemisphere(const vec3& normal) {
    vec3 in_unit_sphere = random_in_unit_sphere();
    if (dot(in_unit_sphere, normal) > 0.0) // In the same hemisphere as the normal
        return in_unit_sphere;
    else
        return -in_unit_sphere;
}

No dependence on the angle from the normal

Writing a new random diffuse function

New code in ray color function :

        point3 target = rec.p + random_in_hemisphere(rec.normal);

Very nuance difference between old diffuse method and the new 



9.1: An Abstract Class For Materials

Different objects with different materials requires a decision in program design.  We could have universal material with lots of different parameters, but in this instance, we are going to abstract out a material class. 

New code: 

#ifndef MATERIAL_H
#define MATERIAL_H

#include "rtweekend.h"

struct hit_record;

class material {
    public:
        virtual bool scatter(
            const ray& r_in, const hit_record& rec, color& attenuation, ray& scattered
        ) const = 0;
};

#endif

Material class


9.2: A Data Structure to Describe Ray-Object Intersections


With this new code implementation we are alerting the compiler that the pointer is pointing towards a  class. 

New code (on hittable.h) :
#include "rtweekend.h"

class material;

New code within struct: 

    shared_ptr<material> mat_ptr;


Material tells us how rays will interact with the surface. When the ray_color() routine gets the hit record it can call member functions of the material pointer to find out what ray, if any, is scattered.


Updated sphere class:

class sphere : public hittable {
    public:
        sphere() {}
        sphere(point3 cen, double r, shared_ptr<material> m)
            : center(cen), radius(r), mat_ptr(m) {};

        virtual bool hit(
            const ray& r, double t_min, double t_max, hit_record& rec) const override;

    public:
        point3 center;
        double radius;
        shared_ptr<material> mat_ptr;
};

bool sphere::hit(const ray& r, double t_min, double t_max, hit_record& rec) const {
    ...

    rec.t = root;
    rec.p = r.at(rec.t);
    vec3 outward_normal = (rec.p - center) / radius;
    rec.set_face_normal(r, outward_normal);
    rec.mat_ptr = mat_ptr;

    return true;
}


New sphere class with added material information 

9.3 : Modeling Light Scatter and Reflectance

In this step of the program we are creating a new vector method to combat if the random unit vector we generate is exactly opposite the normal vector, the two will sum to zero, which is a problem. 

New code: 

class vec3 {
    ...
    bool near_zero() const {
        // Return true if the vector is close to zero in all dimensions.
        const auto s = 1e-8;
        return (fabs(e[0]) < s) && (fabs(e[1]) < s) && (fabs(e[2]) < s);
    }
    ...
};


New vec 3 method that deals with near zero cases 

New code in lambertian class: 

     // Catch degenerate scatter direction
            if (scatter_direction.near_zero())
                scatter_direction = rec.normal;


9.4: Mirrored Light Reflection

Vector math that reflects a ray from a metal mirror

New code:

vec3 reflect(const vec3& v, const vec3& n) {
    return v - 2*dot(v,n)*n;
}

Vector reflection function 

New code (modification of ray color function): 


    if (world.hit(r, 0.001, infinity, rec)) {
        ray scattered;
        color attenuation;
        if (rec.mat_ptr->scatter(r, rec, attenuation, scattered))
            return attenuation * ray_color(scattered, world, depth-1);
        return color(0,0,0);
    }


Scattered reflectance 

9.5 : A Scene with Metal Spheres 

Adding the material spheres we just created into main. This will product a unique and new image output.

New code in main:   

  auto material_ground = make_shared<lambertian>(color(0.8, 0.8, 0.0));
    auto material_center = make_shared<lambertian>(color(0.7, 0.3, 0.3));
    auto material_left   = make_shared<metal>(color(0.8, 0.8, 0.8));
    auto material_right  = make_shared<metal>(color(0.8, 0.6, 0.2));

    world.add(make_shared<sphere>(point3( 0.0, -100.5, -1.0), 100.0, material_ground));
    world.add(make_shared<sphere>(point3( 0.0,    0.0, -1.0),   0.5, material_center));
    world.add(make_shared<sphere>(point3(-1.0,    0.0, -1.0),   0.5, material_left));
    world.add(make_shared<sphere>(point3( 1.0,    0.0, -1.0),   0.5, material_right));



We can modify these r g and b values to change up the colors accordingly. 


9.6: Fuzzy Reflection 

The bigger the sphere the fuzzier the projection will be, so in this part of the program we are going to add a fuzziness parameter. 

New code (modification of fuzziness class) :

class metal : public material {
    public:
        metal(const color& a, double f) : albedo(a), fuzz(f < 1 ? f : 1) {}

        virtual bool scatter(
            const ray& r_in, const hit_record& rec, color& attenuation, ray& scattered
        ) const override {
            vec3 reflected = reflect(unit_vector(r_in.direction()), rec.normal);
            scattered = ray(rec.p, reflected + fuzz*random_in_unit_sphere());
            attenuation = albedo;
            return (dot(scattered.direction(), rec.normal) > 0);
        }

    public:
        color albedo;
        double fuzz;
};

This is a surface that absorbs the fuzziness 


10.1: Refraction 

In this section I learned about refraction. Refraction is the bending of the path of a light wave as it passes across the boundary separating two media, and is caused by changes in speed. 

10.2: Snell's Law


Snells law describes a relationship between the angles of incidence and refraction. In this step of the problem we are going to write a refraction function. 

vec3 refract(const vec3& uv, const vec3& n, double etai_over_etat) {
    auto cos_theta = fmin(dot(-uv, n), 1.0);
    vec3 r_out_perp =  etai_over_etat * (uv + cos_theta*n);
    vec3 r_out_parallel = -sqrt(fabs(1.0 - r_out_perp.length_squared())) * n;
    return r_out_perp + r_out_parallel;
}

auto material_center = make_shared<dielectric>(1.5);
auto material_left   = make_shared<dielectric>(1.5);

In this new code snippet we me make a glass sphere that always refracts, but the image is a bit off

10.3: Total Internal Reflection

Regarding Snells law, it is not always full-proof. If a ray is inside the glass but outside the air, the value of sin cannot be greater than 1. 

New code to determine if the ray can refract: 

if (refraction_ratio * sin_theta > 1.0) {
    // Must Reflect
    ...
} else {
    // Can Refract
    

Dielectric material is a poor conductor of electricity, so we want to write some code that always refracts

New code within class:

            double cos_theta = fmin(dot(-unit_direction, rec.normal), 1.0);
            double sin_theta = sqrt(1.0 - cos_theta*cos_theta);

            bool cannot_refract = refraction_ratio * sin_theta > 1.0;
            vec3 direction;

            if (cannot_refract)
                direction = reflect(unit_direction, rec.normal);
            else
                direction = refract(unit_direction, rec.normal, refraction_ratio);

            scattered = ray(rec.p, direction);

With this new code, our ppm image gives us spheres that sometimes but not always refract. 

 
10.4 Schlick Approximation


The Schlick Approximation is commonly used when dealing with fresnel equations. With this code implementation we are able to mitigate the real glass reflectiveness to produce an image that yields full glass material

New code within dielectric class:

 if (cannot_refract || reflectance(cos_theta, refraction_ratio) > random_double()) 

  private:
        static double reflectance(double cosine, double ref_idx) {
            // Use Schlick's approximation for reflectance.
            auto r0 = (1-ref_idx) / (1+ref_idx);
            r0 = r0*r0;
            return r0 + (1-r0)*pow((1 - cosine),5);
        }


10.5 Modeling a Hollow Glass Sphere

Code snippet:

world.add(make_shared<sphere>(point3(-1.0,    0.0, -1.0),  -0.4, material_left));

This code makes our leftward sphere completely hollow by using a negative radius 


Chapter 11 Positionable Camera

11.1: Camera Viewing Geometry

New code within camera class : 

      camera(
            double vfov, // vertical field-of-view in degrees
            double aspect_ratio
        ) {
            auto theta = degrees_to_radians(vfov);
            auto h = tan(theta/2);
            auto viewport_height = 2.0 * h;
            auto viewport_width = aspect_ratio * viewport_height;

This code now lets the camera have na adjustable field of view

Main implementation: 

    // World

    auto R = cos(pi/4);
    hittable_list world;

    auto material_left  = make_shared<lambertian>(color(0,0,1));
    auto material_right = make_shared<lambertian>(color(1,0,0));

    world.add(make_shared<sphere>(point3(-R, 0, -1), R, material_left));
    world.add(make_shared<sphere>(point3( R, 0, -1), R, material_right));

    // Camera

    camera cam(90.0, aspect_ratio);



This code implementation gives a wide angle view 

11.2: Positioning and Orienting the Camera

The position where we place the camera is called look from, the point that we are pointing to is called lookout

Modified camera class:

   public:
        camera(
            point3 lookfrom,
            point3 lookat,
            vec3   vup,
            double vfov, // vertical field-of-view in degrees
            double aspect_ratio
        ) {
            auto theta = degrees_to_radians(vfov);
            auto h = tan(theta/2);
            auto viewport_height = 2.0 * h;
            auto viewport_width = aspect_ratio * viewport_height;

            auto w = unit_vector(lookfrom - lookat);
            auto u = unit_vector(cross(vup, w));
            auto v = cross(w, u);

            origin = lookfrom;
            horizontal = viewport_width * u;
            vertical = viewport_height * v;
            lower_left_corner = origin - horizontal/2 - vertical/2 - w;
        }

        ray get_ray(double s, double t) const {
            return ray(origin, lower_left_corner + s*horizontal + t*vertical - origin);
        }

Alternative viewpoint :

hittable_list world;

auto material_ground = make_shared<lambertian>(color(0.8, 0.8, 0.0));
auto material_center = make_shared<lambertian>(color(0.1, 0.2, 0.5));
auto material_left   = make_shared<dielectric>(1.5);
auto material_right  = make_shared<metal>(color(0.8, 0.6, 0.2), 0.0);

world.add(make_shared<sphere>(point3( 0.0, -100.5, -1.0), 100.0, material_ground));
world.add(make_shared<sphere>(point3( 0.0,    0.0, -1.0),   0.5, material_center));
world.add(make_shared<sphere>(point3(-1.0,    0.0, -1.0),   0.5, material_left));
world.add(make_shared<sphere>(point3(-1.0,    0.0, -1.0), -0.45, material_left));
world.add(make_shared<sphere>(point3( 1.0,    0.0, -1.0),   0.5, material_right));

camera cam(point3(-2,2,1), point3(0,0,-1), vec3(0,1,0), 90, aspect_ratio);

As demonstrated, this new world list allows us to have a completely different view of the spheres. Pretty awesome and we can modify these lines to our liking. We can zoom out closer or farther.


Chapter 12: Defocus Blur 

The final feature that we are going to implement is a defocus blur

12.1: A Thin Lens Approximation 

We are going to start rays from the lens and send them to a perfectly focused plane


12.2: Generating Sample Rays 


Updated camera class with adjustable depth of field 
class camera {
    public:
        camera(
            point3 lookfrom,
            point3 lookat,
            vec3   vup,
            double vfov, // vertical field-of-view in degrees
            double aspect_ratio,
            double aperture,
            double focus_dist
        ) {
            auto theta = degrees_to_radians(vfov);
            auto h = tan(theta/2);
            auto viewport_height = 2.0 * h;
            auto viewport_width = aspect_ratio * viewport_height;

            w = unit_vector(lookfrom - lookat);
            u = unit_vector(cross(vup, w));
            v = cross(w, u);

            origin = lookfrom;
            horizontal = focus_dist * viewport_width * u;
            vertical = focus_dist * viewport_height * v;
            lower_left_corner = origin - horizontal/2 - vertical/2 - focus_dist*w;

            lens_radius = aperture / 2;
        }


        ray get_ray(double s, double t) const {
            vec3 rd = lens_radius * random_in_unit_disk();
            vec3 offset = u * rd.x() + v * rd.y();

            return ray(
                origin + offset,
                lower_left_corner + s*horizontal + t*vertical - origin - offset
            );
        }

    private:
        point3 origin;
        point3 lower_left_corner;
        vec3 horizontal;
        vec3 vertical;
        vec3 u, v, w;
        double lens_radius;
}; 
Adding a field of depth scene to our camera: 
point3 lookfrom(3,3,2);
point3 lookat(0,0,-1);
vec3 vup(0,1,0);
auto dist_to_focus = (lookfrom-lookat).length();
auto aperture = 2.0;

camera cam(lookfrom, lookat, vup, 20, aspect_ratio, aperture, dist_to_focus);






